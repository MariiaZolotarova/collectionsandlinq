﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinq.Models
{
    public class UserTasks
    {
        public List<TaskModel> Tasks { get; set; }
        public UsersModel User { get; set; }
    }
}
