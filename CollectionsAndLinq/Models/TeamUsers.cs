﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinq.Models
{
    class TeamUsers
    {
        public int? TeamId { get; set; }
        public string Name { get; set; }
        public List<UsersModel> Users { get; set; }
    }
}
