﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLinq.Models
{
    public class NameId
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
