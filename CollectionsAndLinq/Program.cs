﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using CollectionsAndLinq.Models;
using Newtonsoft.Json;

namespace CollectionsAndLinq
{
    class Program
    {
        private static string baseUrl = "https://bsa20.azurewebsites.net/";
        static async Task Main(string[] args)
        {
            var projects = await GetProjects();

            do
            {
                Console.Clear();
                ShowMenu();
                Console.WriteLine("Enter number: ");

                string number = Console.ReadLine();
                switch (number)
                {
                    case "1":
                        Console.WriteLine("Show count of tasks of a specific user");
                        ShowCountProjectsWithTasks(projects);
                        break;
                    case "2":
                        Console.WriteLine("Show tasks designed for a specific user");
                        ShowDesignedTasksForSpecialUser(projects);
                        break;
                    case "3":
                        Console.WriteLine("Show tasks performed in the current (2020) year for a specific user");
                        ShowPerformedTasksInCurrentYear(projects);
                        break;
                    case "4":
                        Console.WriteLine("Show list of teams whose members are older than 10 years");
                        ShowListOfTeamsOlderThan(projects);
                        break;
                    case "5":
                        Console.WriteLine("Show list of users with sorted tasks");
                        ShowListOfUsersWithSortedTasks(projects);
                        break;
                    case "6":
                        Console.WriteLine("Show struct of User" +
                            "\n \t Last user project " +
                            "\n \t The total number of tasks under the last project " +
                            "\n \t The total number of incomplete or canceled tasks for the user " +
                            "\n \t The user's longest task");
                        await StructOfUser(projects);
                        break;
                    case "7":
                        Console.WriteLine("Show struct :" +
                            "\n \t Project" +
                            "\n \t The longest task of the project" +
                            "\n \t The shortest task of the project" +
                            "\n \t The total number of users in the project team");
                        StructOfProjects(projects);
                        break;
                }

                Console.WriteLine("Press any key...");
                Console.ReadKey();
            } while (true);

        }

        private static void ShowCountProjectsWithTasks(List<ProjectModel> projects)
        {
            var userId = ReadUserId();
            var filteredProjects = projects.Where(x => x.AuthorId == userId).ToDictionary(x => x.Name, x => x.Tasks.Count);

            foreach (var projectTask in filteredProjects)
            {
                Console.WriteLine(projectTask.Key + " " + projectTask.Value);
            }
        }

        private static void ShowDesignedTasksForSpecialUser(List<ProjectModel> projects)
        {
            var userId = ReadUserId();
            int lengthName = 45;
            var filtredTasks = projects.Where(x => x.AuthorId == userId)
                .SelectMany(x => x.Tasks)
                .Where(x => x.Name.Length < lengthName && x.PerformerId == userId)
                .ToList();

            foreach (var filtredTask in filtredTasks)
            {
                Console.WriteLine(filtredTask.Name);
            }
        }

        private static void ShowPerformedTasksInCurrentYear(List<ProjectModel> projects)
        {
            var userId = ReadUserId();
            var filtredTasks = projects.Where(x => x.AuthorId == userId)
                                              .SelectMany(x => x.Tasks)
                                              .Where(x => x.FinishedAt.Year == DateTime.UtcNow.Year
                                              && x.State == StateEnum.Finished
                                              && x.PerformerId == userId)
                                              .Select(x => new NameId
                                              {
                                                  Id = x.Id,
                                                  Name = x.Name
                                              }).ToList();

            foreach (var filtredTask in filtredTasks)
            {
                Console.WriteLine(filtredTask.Id + " " + filtredTask.Name);
            }
        }

        private static void ShowListOfTeamsOlderThan(List<ProjectModel> projects)
        {
            var filtredUsers = projects
                .GroupBy(x => new
                {
                    x.TeamId
                })
                .Select(x => new TeamUsers
                {
                    TeamId = x.Key.TeamId,
                    Name = x.First().Name,
                    Users = x.First().Team.Users.Where(x => x.Birthday.Year <= DateTime.UtcNow.Year - 10).OrderByDescending(y => y.RegistredAt).ToList()
                }).ToList();

            foreach (var userTeam in filtredUsers)
            {
                Console.WriteLine(userTeam.TeamId + " " + userTeam.Name + " " + string.Join(",", userTeam.Users.Select(x => x.Email)));
            }
        }

        private static void ShowListOfUsersWithSortedTasks(List<ProjectModel> projects)
        {
            var usersTasks = projects.SelectMany(x => x.Tasks).GroupBy(x => x.PerformerId).Select(x => new UserTasks
            {
                User = x.First(c => c.PerformerId == x.Key).Performer,
                Tasks = x.OrderByDescending(m => m.Name.Length).ToList()
            }).OrderBy(v => v.User.FirstName).ToList();

            foreach (var userTasks in usersTasks)
            {
                Console.WriteLine($"FirstName: {userTasks.User.FirstName}, Tasks: [{string.Join(", ", userTasks.Tasks.Select(x => $"(Id: {x.Id}, Length: {x.Name.Length})"))}]");
            }

        }

        private static async Task StructOfUser(List<ProjectModel> projects)
        {
            var userId = ReadUserId();
            var user = await GetDataFromEndpointAsync<UsersModel>($"Users/{userId}");
            var lastUserProject = projects.Where(x => x.AuthorId == userId)
                .Select(x => new ProjectTasks
                {
                    Project = x,
                    TasksCount = x.Tasks.Count(x => x.PerformerId == userId),
                    NotFinishedTasks = x.Tasks.Count(x => x.PerformerId == userId && x.State != StateEnum.Finished),
                    LongestTask = x.Tasks.Any() ? x.Tasks.Aggregate((i1, i2) => (i1.FinishedAt.Ticks - i1.CreatedAt.Ticks) > (i2.FinishedAt.Ticks - i2.CreatedAt.Ticks) ? i1 : i2) : null
                }).OrderByDescending(x => x.Project.CreatedAt).FirstOrDefault();
 

            if (lastUserProject == null)
            {
                Console.WriteLine("No current projects");
            }
            else
            {
                lastUserProject.User = user;
                Console.WriteLine($"User: {lastUserProject.User.Email}");
                Console.WriteLine($"LastProject: {lastUserProject.Project.Name}");
                Console.WriteLine($"TasksCOunt: {lastUserProject.TasksCount}");
                Console.WriteLine($"NotFinishedTasks: {lastUserProject.NotFinishedTasks}");
                Console.WriteLine($"LongestTask: {lastUserProject.LongestTask}");
            }
        }

        private static void StructOfProjects(List<ProjectModel> projects)
        {
            var newProjects = projects.Select(x => new
            {
                Project = x,
                LongestTask = x.Tasks.Any() ? x.Tasks.Aggregate((i1, i2) => i1.Description.Length > i2.Description.Length ? i1 : i2) : null,
                ShortestTask = x.Tasks.Any() ? x.Tasks.Aggregate((i1, i2) => i1.Name.Length < i2.Name.Length ? i1 : i2) : null,
                UsersCount = x.Description.Length > 20 || x.Tasks.Count < 3 ? x.Team.Users.Count : 0
            });

            foreach(var project in newProjects)
            {
                Console.WriteLine($"Project: {project.Project.Name}");
                Console.WriteLine($"LongestTask: {project.LongestTask?.Name}");
                Console.WriteLine($"ShortestTask: {project.ShortestTask?.Name}");
                Console.WriteLine($"UsersCount: {project.UsersCount}");
                Console.WriteLine("-----------------------------------------------");
            }
        }

        private static async Task<List<ProjectModel>> GetProjects()
        {
            var users = await GetDataFromEndpointAsync<List<UsersModel>>("Users");
            var tasks = await GetDataFromEndpointAsync<List<TaskModel>>("Tasks");
            var projects = await GetDataFromEndpointAsync<List<ProjectModel>>("Projects");
            var teams = await GetDataFromEndpointAsync<List<TeamsModel>>("Teams");

            var joinedProjects = projects.Join(users, project => project.AuthorId, user => user.Id, (project, user) =>
            {
                project.Author = user;
                return project;
            }).Join(teams, project => project.TeamId, team => team.Id, (project, team) =>
            {
                team.Users = users.Where(x => x.TeamId == team.Id).ToList();
                project.Team = team;
                return project;
            }).GroupJoin(tasks, project => project.Id, task => task.ProjectId, (project, task) =>
            {
                project.Tasks = task.Join(users, task => task.PerformerId, user => user.Id, (task, user) =>
                {
                    task.Performer = user;
                    return task;
                }).ToList();
                return project;
            }).ToList();

            return joinedProjects;
        }

        private static async Task<T> GetDataFromEndpointAsync<T>(string endpoint)
        {
            using var client = new HttpClient();
            var response = await client.GetAsync(baseUrl + "api/" + endpoint);
            var content = await response.Content.ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<T>(content);
            return model;
        }

        private static int ReadUserId()
        {
            Console.WriteLine("Enter User Id");
            var parseResult = Int32.TryParse(Console.ReadLine(), out var userId);

            if (!parseResult)
            {
                Console.WriteLine("Wrong user id");
            }

            return userId;
        }

        private static void ShowMenu()
        {
            Console.WriteLine("1 - Show count of tasks of a specific user");
            Console.WriteLine("2 - Show tasks designed for a specific user");
            Console.WriteLine("3 - Show tasks performed in the current (2020) year for a specific user");
            Console.WriteLine("4 - Show list of teams whose members are older than 10 years");
            Console.WriteLine("5 - Show list of users with sorted tasks");
            Console.WriteLine("6 - Show struct of user:" +
                "\n \t Last user project " +
                "\n \t The total number of tasks under the last project " +
                "\n \t The total number of incomplete or canceled tasks for the user " +
                "\n \t The user's longest task");
            Console.WriteLine("7 - Show struct :" +
                           "\n \t Project" +
                           "\n \t The longest task of the project" +
                           "\n \t The shortest task of the project" +
                           "\n \t The total number of users in the project team");
        }
    }
}
